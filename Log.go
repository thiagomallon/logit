package logit

import (
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"time"
)

type jsonLog struct {
	Category string `json:"category"`
	Message  string `json:"message"`
	Trace    string `json:"trace"`
}

type SysLog struct {
	file       *os.File
	categories map[string][]string
}

func NewSysLog() *SysLog {
	lg := SysLog{}
	lg.SetPath(fmt.Sprintf("%s%s%s", "logs/", time.Now().Format("2006_01_02"), ".log"))
	lg.chargeCategories()
	return &lg
}

func (lg *SysLog) SetPath(filePath string) {
	lg.file, _ = os.OpenFile(filePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 1444)
}

func (lg *SysLog) chargeCategories() {
	lg.categories = map[string][]string{
		"emergency": {"Emergency:", "an emergency"},
		"alert":     {"Alert:", "an alert"},
		"critical":  {"Critical:", "a critical"},
		"error":     {"Error:", "an error"},
		"warning":   {"Warning:", "a warning"},
		"notice":    {"Notice:", "a notice"},
		"info":      {"Info:", "an info"},
		"debug":     {"Debug:", "a debug"},
	}
}

func (lg *SysLog) AppendCategories(newCategories map[string][]string) {
	for k, v := range newCategories {
		lg.categories[k] = v
	}
}

func (lg *SysLog) WJsonLog(category string, msg string, trace string) {
	val, res := lg.categories[category]
	if !res {
		fmt.Println("The log category does not exists: ", lg.GetTraceMsg())
	} else {
		logObj := &jsonLog{Category: category, Message: msg, Trace: trace}
		jsonLogMsg, _ := json.Marshal(logObj)

		logMsg := []byte(fmt.Sprintf("%s%s", string(jsonLogMsg), "\n"))
		_, err := lg.file.Write(logMsg)
		if err != nil {
			fmt.Println("Error while trying to log", val[1], " on system ", lg.GetTraceMsg())
		}
	}
}

func (lg *SysLog) WLog(category string, msg string) {
	val, res := lg.categories[category]
	if !res {
		fmt.Println("The log category does not exists: ", lg.GetTraceMsg())
	} else {
		logMsg := []byte(val[0])        // convert string to byte slice
		logMsg = append(logMsg, msg...) // append byte to slice
		_, err := lg.file.Write(logMsg)
		if err != nil {
			fmt.Println("Error while trying to log:", category, " - ", lg.GetTraceMsg())
		}
	}
}

func (lg *SysLog) GetTraceMsg() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return fmt.Sprintf("%s,:%d %s\n", frame.File, frame.Line, frame.Function)
}

func (lg *SysLog) close() {
	err := lg.file.Close()
	if err != nil {
		fmt.Println("Error while trying to close the log file pointer:", lg.GetTraceMsg())
	}
}
